package br.com.ozeano.curso.api.bb.domain.model;

public enum TipoPagamento {

	BOLETO, CARTAO;
	
}
