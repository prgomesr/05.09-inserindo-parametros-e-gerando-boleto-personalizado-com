package br.com.ozeano.curso.api.bb.domain.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Empresa extends BaseEntity {

	private String razaoSocial;
	private String cnpj;
	
	@Embedded
	private Endereco endereco;
	
}
