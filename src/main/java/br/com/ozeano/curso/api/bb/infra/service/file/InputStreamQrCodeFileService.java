package br.com.ozeano.curso.api.bb.infra.service.file;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.springframework.stereotype.Service;

import br.com.ozeano.curso.api.bb.domain.service.file.GeradorQrCodeService;
import net.glxn.qrgen.javase.QRCode;

@Service
public class InputStreamQrCodeFileService implements GeradorQrCodeService {

	@Override
	public InputStream gerar(String codigo) {
		var code = QRCode.from(codigo).withSize(250, 250).stream();
		ByteArrayInputStream bis = new ByteArrayInputStream(code.toByteArray());
		
		return bis;
	}

}
