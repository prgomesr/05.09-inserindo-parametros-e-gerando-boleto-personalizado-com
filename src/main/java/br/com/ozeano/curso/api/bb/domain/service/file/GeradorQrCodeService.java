package br.com.ozeano.curso.api.bb.domain.service.file;

import java.io.InputStream;

public interface GeradorQrCodeService {

	InputStream gerar(String codigo);
	
}
