package br.com.ozeano.curso.api.bb.infra.service.file;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.caelum.stella.boleto.transformer.GeradorDeBoleto;
import br.com.ozeano.curso.api.bb.domain.model.Fatura;
import br.com.ozeano.curso.api.bb.domain.repository.FaturaRegistradaRepository;
import br.com.ozeano.curso.api.bb.domain.service.file.GeradorDeBoletoService;
import br.com.ozeano.curso.api.bb.domain.service.file.GeradorQrCodeService;
import br.com.ozeano.curso.api.bb.infra.model.input.CobrancaInput;

@Service
public class PdfBoletoFileService implements GeradorDeBoletoService {

	@Autowired
	private GeradorQrCodeService geradorQrcode;
	
	@Autowired
	private FaturaRegistradaRepository faturaRegistradaRepository;
	
	@Override
	public byte[] gerar(Fatura fatura, CobrancaInput cobranca) {

		var boleto = criarBoleto(fatura, cobranca);
		
		var pathBoletoPersonalizado = this.getClass().getResourceAsStream("/relatorios/boleto-personalizado.jasper");
		var pathLogo = this.getClass().getResourceAsStream("/relatorios/logo-curso-api-bb.png");
		var faturaRegistrada = faturaRegistradaRepository.buscarPorFaturaId(fatura.getId());
		
		var parametros = new HashMap<String, Object>();
		parametros.put("QRCODE", geradorQrcode.gerar(faturaRegistrada.getQrcodeEmv()));
		parametros.put("LOGO", pathLogo);
		

		var gerador = new GeradorDeBoleto(pathBoletoPersonalizado, parametros, boleto);

		return gerador.geraPDF();
	}

}
