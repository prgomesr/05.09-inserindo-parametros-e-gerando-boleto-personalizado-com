package br.com.ozeano.curso.api.bb.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ozeano.curso.api.bb.domain.model.FaturaRegistrada;

@Repository
public interface FaturaRegistradaRepository extends JpaRepository<FaturaRegistrada, Long> {

	@Query("from FaturaRegistrada where fatura.id = :faturaId")
	FaturaRegistrada buscarPorFaturaId(Long faturaId);

}
